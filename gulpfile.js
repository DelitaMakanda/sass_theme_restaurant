// Include gulp
var gulp    = require('gulp');

// Include other required plugins
var sass    = require('gulp-sass'),
sourcemaps  = require('gulp-sourcemaps');

// Loop sass theme files and output separately.
gulp.task('sass-theme', function () {
return gulp.src('./assets/sass/themes/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({
	outputStyle: 'expanded',
	}))
	.on('error', sass.logError)
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('./assets/css/themes'))
});

// Compile main sass file
gulp.task('sass', function () {
return gulp.src([
		'./assets/sass/main.scss',
	])
	.pipe(sourcemaps.init())
	.pipe(sass({
		outputStyle: 'expanded',
	}))
	.on('error', sass.logError)
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('./assets/css'))
});


// Watch Files For Changes
gulp.task('watch', function() {
	gulp.watch('./assets/sass/**/*.scss', ['sass', 'sass-theme']);
});



